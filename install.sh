sudo rm -r /home/shinyproxy/
sudo mkdir /home/shinyproxy/

sudo cp -r apps /home/shinyproxy
sudo cp -r dockerfiles /home/shinyproxy
sudo cp -r scripts /home/shinyproxy
sudo cp -r palo /home/shinyproxy
sudo cp -r R /home/shinyproxy

cd /home/shinyproxy/dockerfiles/shinyapp
sudo docker build -t gpn-app .

cd /home/shinyproxy/dockerfiles/shinyproxy
sudo docker network create shinyproxy-net
sudo docker build . -t shinyproxy
sudo docker run -d -v /var/run/docker.sock:/var/run/docker.sock --net shinyproxy-net -p 8080:8080 shinyproxy

sudo docker pull zkhadikov/palo
sudo docker pull rocker/rstudio

